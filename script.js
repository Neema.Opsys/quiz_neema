let cheatmodeactive = true

const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question')
const answerButtonsElement = document.getElementById('answer-buttons')
const timerElement = document.getElementById('timer-btn')



let shuffledQuestions, currentQuestionIndex


// A fucntion to Toggle Cheat Mode
function togglecheats() {
    if (cheatmodeactive) {
        cheatmodeactive = false
    } else {
        cheatmodeactive = true
    }
}


startButton.addEventListener('click', startGame)
//This will start the timer
timerDefaultValue = 3;
timerCurrentValue = 0;
timerTick = null;



function startTimer() {
    console.log('Starting timer');
    if (timerCurrentValue <= 0) resetTimer();
    timerTick = setInterval(tickTimer, 1000);
}


// This will stop the timer

function stopTimer() {
    console.log('stopTimer');
    clearInterval(timerTick)
}

//This will occure everysecond i.e 'Tick'
function tickTimer() {
    console.log('Timer tick. Value = ' + timerCurrentValue);
    // logic to check if zero
    if (timerCurrentValue <= 0) timerZeroHit();

    // Display the current Value of the Timer in the timer-btn element
    var timerBtnElement = document.getElementById('timer-btn');
    timerBtnElement.innerHTML = "Time Left:" + timerCurrentValue
    timerCurrentValue = timerCurrentValue - 1;
}

//This will reset the timer back to the default value
function resetTimer() {
    console.log('restTimer');
    timerCurrentValue = timerDefaultValue;
}

function timerZeroHit() {
    console.log('timerZeroHit');
    stopTimer();

    currentQuestionIndex++
    setNextQuestion()
    resetTimer()
    startTimer()
}

function setNextQuestion() {
    resetState('setNextQuestion')
    showQuestion(shuffledQuestions[currentQuestionIndex])

}

nextButton.addEventListener('click', () => {
    currentQuestionIndex++
    setNextQuestion()
    resetTimer()
    startTimer()
})

function startGame() {
    startButton.classList.add('hide') // Hide the Start Button
    timerElement.classList.remove('hide')

    //Start Timer after click next button 
    startTimer()
    shuffledQuestions = questions.sort(() => Math.random() - .5) // Randomize the Question List
    currentQuestionIndex = 0
    questionContainerElement.classList.remove('hide') // Show the Question Container
    setNextQuestion()

}
/**
 * 
 * @param {question} question 
 */
function showQuestion(question) {
    questionElement.innerText = question.question
    question.answers.forEach(answer => {
        const button = document.createElement('button')
        button.innerText = answer.text
        button.classList.add('btn')

        if (answer.correct) {
            if (cheatmodeactive) {
                console.log(answer.text)
            }
            button.dataset.correct = answer.correct
        }
        button.addEventListener('click', selectAnswer)
        answerButtonsElement.appendChild(button)
    })
}


/**
 * @param{element} element The HTML element we want to target
@param{property} Node.firstChild: ChildNode
@returns first child
 */
function resetState() {
    clearStatusClass(document.body)
    nextButton.classList.add('hide')
    while (answerButtonsElement.firstChild) {
        answerButtonsElement.removeChild(answerButtonsElement.firstChild)
    }
}


/**
 * 
 * @param {*} e 
 * @param {HTMLElement} element The HTML Element we want to target
 * @param {HTMLElement} correct The HTML Element 
 */
function selectAnswer(e) {
    if (selectAnswer) { //Stop timer after selecting the answer
        stopTimer()
    }

    const selectedButton = e.target
    const correct = selectedButton.dataset.correct

    setStatusClass(document.body, correct)
    Array.from(answerButtonsElement.children).forEach(button => {
        setStatusClass(button, button.dataset.correct)
    })
    if (shuffledQuestions.length > currentQuestionIndex + 1) {
        nextButton.classList.remove('hide')
    } else {
        startButton.innerText = 'Restart'
        startButton.classList.remove('hide')
    }
}

/**
 * 
 * @param {HTMLElement} element The HTML Element we want to target
 * @param {HTMLElement} correct HTML Element
 */
function setStatusClass(element, correct) {
    clearStatusClass(element)
    if (correct) {
        element.classList.add('correct')
    } else {
        element.classList.add('wrong')
    }
}

/**
 *
 * @param {HTMLElement} element The HTML Element we want to target
 */
function clearStatusClass(element) {
    element.classList.remove('correct')
    element.classList.remove('wrong')
}



/**
 * 
 * @param {number} number A number
 * @param {string} string A string
 * @returns string
 */
function test(number, string) {
    return number + string;
}

/**
 * - Cheat Mode | Console Command that would toggle debugging to the console so when a question is loaded it prints out the correct answer
 * - Debug Information | When the applicaiton loads, print statistics to the console eg. Number of Questions Loaded, Current Date/Time
    # Declare Global Varible
    - Function to toggle the mode (ie. false to true and true to false)
    - Will need to find the appropriate place to print the answer out (print per question)
        - Log the answer to the console

    Loaded 4 Questions
    6/9/2021 10:38.03am -> Seconds Optional
    Timed Questions
 */




const questions = [{
        question: 'What is 2 + 2?',
        answers: [{
                text: '4',
                correct: true
            },
            {
                text: '22',
                correct: false
            }
        ]
    },
    {
        question: 'Who is the best YouTuber?',
        answers: [{
                text: 'Web Dev Simplified',
                correct: true
            },
            {
                text: 'Traversy Media',
                correct: true
            },
            {
                text: 'Dev Ed',
                correct: true
            },
            {
                text: 'Fun Fun Function',
                correct: true
            }
        ]
    },
    {
        question: 'Is web development fun?',
        answers: [{
                text: 'Kinda',
                correct: false
            },
            {
                text: 'YES!!!',
                correct: true
            },
            {
                text: 'Um no',
                correct: false
            },
            {
                text: 'IDK',
                correct: false
            }
        ]
    },
    {
        question: 'What is 4 * 2?',
        answers: [{
                text: '6',
                correct: false
            },
            {
                text: '8',
                correct: true
            }
        ]
    }
]
var date = new Date()
console.log(date)
console.log(questions.length)
